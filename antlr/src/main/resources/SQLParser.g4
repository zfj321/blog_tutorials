parser grammar SQLParser;

@header {
package com.example.parser;
}

options { tokenVocab=SQLLexer; }

// Entry rule
sql: select_stmt SEMICOLON? ;

// Select statement rule
select_stmt: SELECT select_list FROM table_name where_clause? order_clause? ;

// Select list rule
select_list: STAR | column_list ;

// Column list rule
column_list: column_name (COMMA column_name)* ;

// Table name rule
table_name: ID ;

// Where clause rule
where_clause: WHERE condition (AND condition | OR condition)* ;

// Order clause rule
order_clause: ORDER BY order_element (COMMA order_element)* ;

// Order element rule
order_element: column_name (DESC)? ;

// Condition rule
condition: column_name EQUAL value ;

// Column name rule
column_name: ID ;

// Value rule
value: STRING | INT ;