package com.spring.annotationtest;

import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;

public class AnnotationProcessor {
    public static void main(String[] args) {
        Class<MyServiceImpl> clazz = MyServiceImpl.class;

        // 递归查找注解
        AnnotationAttributes attributes = AnnotatedElementUtils.getMergedAnnotationAttributes(clazz, MyService.class);
        if (attributes != null) {
            String value = attributes.getString("value");
            System.out.println("MyService value: " + value);
        }

        // 查找组合注解中的元注解
        Component component = AnnotatedElementUtils.findMergedAnnotation(clazz, Component.class);
        if (component != null) {
            System.out.println("Found Component annotation with value: " + component.value());
        }
    }
}