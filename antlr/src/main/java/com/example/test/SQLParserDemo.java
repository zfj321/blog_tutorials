package com.example.test;

import com.example.parser.SQLLexer;
import com.example.parser.SQLParser;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;



public class SQLParserDemo {
    public static void main(String[] args) throws Exception {
        String sql = "SELECT * FROM t_user WHERE name='tom' ORDER BY id DESC";

        // Create a lexer that feeds off of input CharStream
        SQLLexer lexer = new SQLLexer(CharStreams.fromString(sql));

        // Create a buffer of tokens pulled from the lexer
        CommonTokenStream tokens = new CommonTokenStream(lexer);

        // Create a parser that feeds off the tokens buffer
        SQLParser parser = new SQLParser(tokens);

        // Begin parsing at the root rule
        ParseTree tree = parser.sql();

        // Print the parse tree
        System.out.println(tree.toStringTree(parser));
    }
}