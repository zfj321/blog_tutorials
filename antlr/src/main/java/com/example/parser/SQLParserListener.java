// Generated from java-escape by ANTLR 4.11.1

package com.example.parser;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link SQLParser}.
 */
public interface SQLParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link SQLParser#sql}.
	 * @param ctx the parse tree
	 */
	void enterSql(SQLParser.SqlContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#sql}.
	 * @param ctx the parse tree
	 */
	void exitSql(SQLParser.SqlContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#select_stmt}.
	 * @param ctx the parse tree
	 */
	void enterSelect_stmt(SQLParser.Select_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#select_stmt}.
	 * @param ctx the parse tree
	 */
	void exitSelect_stmt(SQLParser.Select_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#select_list}.
	 * @param ctx the parse tree
	 */
	void enterSelect_list(SQLParser.Select_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#select_list}.
	 * @param ctx the parse tree
	 */
	void exitSelect_list(SQLParser.Select_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#column_list}.
	 * @param ctx the parse tree
	 */
	void enterColumn_list(SQLParser.Column_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#column_list}.
	 * @param ctx the parse tree
	 */
	void exitColumn_list(SQLParser.Column_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#table_name}.
	 * @param ctx the parse tree
	 */
	void enterTable_name(SQLParser.Table_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#table_name}.
	 * @param ctx the parse tree
	 */
	void exitTable_name(SQLParser.Table_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#where_clause}.
	 * @param ctx the parse tree
	 */
	void enterWhere_clause(SQLParser.Where_clauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#where_clause}.
	 * @param ctx the parse tree
	 */
	void exitWhere_clause(SQLParser.Where_clauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#order_clause}.
	 * @param ctx the parse tree
	 */
	void enterOrder_clause(SQLParser.Order_clauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#order_clause}.
	 * @param ctx the parse tree
	 */
	void exitOrder_clause(SQLParser.Order_clauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#order_element}.
	 * @param ctx the parse tree
	 */
	void enterOrder_element(SQLParser.Order_elementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#order_element}.
	 * @param ctx the parse tree
	 */
	void exitOrder_element(SQLParser.Order_elementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#condition}.
	 * @param ctx the parse tree
	 */
	void enterCondition(SQLParser.ConditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#condition}.
	 * @param ctx the parse tree
	 */
	void exitCondition(SQLParser.ConditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#column_name}.
	 * @param ctx the parse tree
	 */
	void enterColumn_name(SQLParser.Column_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#column_name}.
	 * @param ctx the parse tree
	 */
	void exitColumn_name(SQLParser.Column_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#value}.
	 * @param ctx the parse tree
	 */
	void enterValue(SQLParser.ValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#value}.
	 * @param ctx the parse tree
	 */
	void exitValue(SQLParser.ValueContext ctx);
}