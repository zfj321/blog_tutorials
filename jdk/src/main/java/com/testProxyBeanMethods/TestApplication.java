package com.testProxyBeanMethods;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@SpringBootApplication
public class TestApplication {

    public static void main(String[] args) throws Exception {
        ConfigurableApplicationContext ctx = SpringApplication.run(TestApplication.class, args);
        while (true) {
            ctx.getBean(HelloConfiguration.class).test();
            Thread.sleep(1000L);
        }
    }
}


@Configuration(proxyBeanMethods = false)
class HelloConfiguration {

    //spring 容器托管的bean
    @Bean("person")
    //Scope可以不用加，默认就是单例
    public Person person() {
        System.out.println("===:make person " + System.currentTimeMillis());
        return new Person();
    }

    /**
     * 测试直接调用person()方法
     */
    public void test() {
        Person p = person();
        System.out.println("+++person hash code:" + System.identityHashCode(p));
        System.out.println("+++person name:" + p.getName());
    }

}


/**
 * 定义一个class
 */
class Person {
    private String name;

    private static int i = 0;

    public Person() {
        name = String.valueOf(++i);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
