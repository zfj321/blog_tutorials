// Generated from java-escape by ANTLR 4.11.1

    package com.example.parser;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast", "CheckReturnValue"})
public class SQLLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.11.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		SELECT=1, FROM=2, WHERE=3, ORDER=4, BY=5, DESC=6, AND=7, OR=8, EQUAL=9, 
		STAR=10, COMMA=11, SEMICOLON=12, QUOTE=13, ID=14, INT=15, STRING=16, WS=17;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"SELECT", "FROM", "WHERE", "ORDER", "BY", "DESC", "AND", "OR", "EQUAL", 
			"STAR", "COMMA", "SEMICOLON", "QUOTE", "ID", "INT", "STRING", "WS"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, "'='", "'*'", "','", 
			"';'", "'''"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "SELECT", "FROM", "WHERE", "ORDER", "BY", "DESC", "AND", "OR", 
			"EQUAL", "STAR", "COMMA", "SEMICOLON", "QUOTE", "ID", "INT", "STRING", 
			"WS"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public SQLLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "SQLLexer.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\u0004\u0000\u0011r\u0006\uffff\uffff\u0002\u0000\u0007\u0000\u0002\u0001"+
		"\u0007\u0001\u0002\u0002\u0007\u0002\u0002\u0003\u0007\u0003\u0002\u0004"+
		"\u0007\u0004\u0002\u0005\u0007\u0005\u0002\u0006\u0007\u0006\u0002\u0007"+
		"\u0007\u0007\u0002\b\u0007\b\u0002\t\u0007\t\u0002\n\u0007\n\u0002\u000b"+
		"\u0007\u000b\u0002\f\u0007\f\u0002\r\u0007\r\u0002\u000e\u0007\u000e\u0002"+
		"\u000f\u0007\u000f\u0002\u0010\u0007\u0010\u0001\u0000\u0001\u0000\u0001"+
		"\u0000\u0001\u0000\u0001\u0000\u0001\u0000\u0001\u0000\u0001\u0001\u0001"+
		"\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0002\u0001\u0002\u0001"+
		"\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0003\u0001\u0003\u0001"+
		"\u0003\u0001\u0003\u0001\u0003\u0001\u0003\u0001\u0004\u0001\u0004\u0001"+
		"\u0004\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001"+
		"\u0006\u0001\u0006\u0001\u0006\u0001\u0006\u0001\u0007\u0001\u0007\u0001"+
		"\u0007\u0001\b\u0001\b\u0001\t\u0001\t\u0001\n\u0001\n\u0001\u000b\u0001"+
		"\u000b\u0001\f\u0001\f\u0001\r\u0001\r\u0005\rW\b\r\n\r\f\rZ\t\r\u0001"+
		"\u000e\u0004\u000e]\b\u000e\u000b\u000e\f\u000e^\u0001\u000f\u0001\u000f"+
		"\u0001\u000f\u0001\u000f\u0005\u000fe\b\u000f\n\u000f\f\u000fh\t\u000f"+
		"\u0001\u000f\u0001\u000f\u0001\u0010\u0004\u0010m\b\u0010\u000b\u0010"+
		"\f\u0010n\u0001\u0010\u0001\u0010\u0000\u0000\u0011\u0001\u0001\u0003"+
		"\u0002\u0005\u0003\u0007\u0004\t\u0005\u000b\u0006\r\u0007\u000f\b\u0011"+
		"\t\u0013\n\u0015\u000b\u0017\f\u0019\r\u001b\u000e\u001d\u000f\u001f\u0010"+
		"!\u0011\u0001\u0000\u0015\u0002\u0000SSss\u0002\u0000EEee\u0002\u0000"+
		"LLll\u0002\u0000CCcc\u0002\u0000TTtt\u0002\u0000FFff\u0002\u0000RRrr\u0002"+
		"\u0000OOoo\u0002\u0000MMmm\u0002\u0000WWww\u0002\u0000HHhh\u0002\u0000"+
		"DDdd\u0002\u0000BBbb\u0002\u0000YYyy\u0002\u0000AAaa\u0002\u0000NNnn\u0003"+
		"\u0000AZ__az\u0004\u000009AZ__az\u0001\u000009\u0003\u0000\n\n\r\r\'\'"+
		"\u0003\u0000\t\n\r\r  v\u0000\u0001\u0001\u0000\u0000\u0000\u0000\u0003"+
		"\u0001\u0000\u0000\u0000\u0000\u0005\u0001\u0000\u0000\u0000\u0000\u0007"+
		"\u0001\u0000\u0000\u0000\u0000\t\u0001\u0000\u0000\u0000\u0000\u000b\u0001"+
		"\u0000\u0000\u0000\u0000\r\u0001\u0000\u0000\u0000\u0000\u000f\u0001\u0000"+
		"\u0000\u0000\u0000\u0011\u0001\u0000\u0000\u0000\u0000\u0013\u0001\u0000"+
		"\u0000\u0000\u0000\u0015\u0001\u0000\u0000\u0000\u0000\u0017\u0001\u0000"+
		"\u0000\u0000\u0000\u0019\u0001\u0000\u0000\u0000\u0000\u001b\u0001\u0000"+
		"\u0000\u0000\u0000\u001d\u0001\u0000\u0000\u0000\u0000\u001f\u0001\u0000"+
		"\u0000\u0000\u0000!\u0001\u0000\u0000\u0000\u0001#\u0001\u0000\u0000\u0000"+
		"\u0003*\u0001\u0000\u0000\u0000\u0005/\u0001\u0000\u0000\u0000\u00075"+
		"\u0001\u0000\u0000\u0000\t;\u0001\u0000\u0000\u0000\u000b>\u0001\u0000"+
		"\u0000\u0000\rC\u0001\u0000\u0000\u0000\u000fG\u0001\u0000\u0000\u0000"+
		"\u0011J\u0001\u0000\u0000\u0000\u0013L\u0001\u0000\u0000\u0000\u0015N"+
		"\u0001\u0000\u0000\u0000\u0017P\u0001\u0000\u0000\u0000\u0019R\u0001\u0000"+
		"\u0000\u0000\u001bT\u0001\u0000\u0000\u0000\u001d\\\u0001\u0000\u0000"+
		"\u0000\u001f`\u0001\u0000\u0000\u0000!l\u0001\u0000\u0000\u0000#$\u0007"+
		"\u0000\u0000\u0000$%\u0007\u0001\u0000\u0000%&\u0007\u0002\u0000\u0000"+
		"&\'\u0007\u0001\u0000\u0000\'(\u0007\u0003\u0000\u0000()\u0007\u0004\u0000"+
		"\u0000)\u0002\u0001\u0000\u0000\u0000*+\u0007\u0005\u0000\u0000+,\u0007"+
		"\u0006\u0000\u0000,-\u0007\u0007\u0000\u0000-.\u0007\b\u0000\u0000.\u0004"+
		"\u0001\u0000\u0000\u0000/0\u0007\t\u0000\u000001\u0007\n\u0000\u00001"+
		"2\u0007\u0001\u0000\u000023\u0007\u0006\u0000\u000034\u0007\u0001\u0000"+
		"\u00004\u0006\u0001\u0000\u0000\u000056\u0007\u0007\u0000\u000067\u0007"+
		"\u0006\u0000\u000078\u0007\u000b\u0000\u000089\u0007\u0001\u0000\u0000"+
		"9:\u0007\u0006\u0000\u0000:\b\u0001\u0000\u0000\u0000;<\u0007\f\u0000"+
		"\u0000<=\u0007\r\u0000\u0000=\n\u0001\u0000\u0000\u0000>?\u0007\u000b"+
		"\u0000\u0000?@\u0007\u0001\u0000\u0000@A\u0007\u0000\u0000\u0000AB\u0007"+
		"\u0003\u0000\u0000B\f\u0001\u0000\u0000\u0000CD\u0007\u000e\u0000\u0000"+
		"DE\u0007\u000f\u0000\u0000EF\u0007\u000b\u0000\u0000F\u000e\u0001\u0000"+
		"\u0000\u0000GH\u0007\u0007\u0000\u0000HI\u0007\u0006\u0000\u0000I\u0010"+
		"\u0001\u0000\u0000\u0000JK\u0005=\u0000\u0000K\u0012\u0001\u0000\u0000"+
		"\u0000LM\u0005*\u0000\u0000M\u0014\u0001\u0000\u0000\u0000NO\u0005,\u0000"+
		"\u0000O\u0016\u0001\u0000\u0000\u0000PQ\u0005;\u0000\u0000Q\u0018\u0001"+
		"\u0000\u0000\u0000RS\u0005\'\u0000\u0000S\u001a\u0001\u0000\u0000\u0000"+
		"TX\u0007\u0010\u0000\u0000UW\u0007\u0011\u0000\u0000VU\u0001\u0000\u0000"+
		"\u0000WZ\u0001\u0000\u0000\u0000XV\u0001\u0000\u0000\u0000XY\u0001\u0000"+
		"\u0000\u0000Y\u001c\u0001\u0000\u0000\u0000ZX\u0001\u0000\u0000\u0000"+
		"[]\u0007\u0012\u0000\u0000\\[\u0001\u0000\u0000\u0000]^\u0001\u0000\u0000"+
		"\u0000^\\\u0001\u0000\u0000\u0000^_\u0001\u0000\u0000\u0000_\u001e\u0001"+
		"\u0000\u0000\u0000`f\u0003\u0019\f\u0000ae\b\u0013\u0000\u0000bc\u0005"+
		"\'\u0000\u0000ce\u0005\'\u0000\u0000da\u0001\u0000\u0000\u0000db\u0001"+
		"\u0000\u0000\u0000eh\u0001\u0000\u0000\u0000fd\u0001\u0000\u0000\u0000"+
		"fg\u0001\u0000\u0000\u0000gi\u0001\u0000\u0000\u0000hf\u0001\u0000\u0000"+
		"\u0000ij\u0003\u0019\f\u0000j \u0001\u0000\u0000\u0000km\u0007\u0014\u0000"+
		"\u0000lk\u0001\u0000\u0000\u0000mn\u0001\u0000\u0000\u0000nl\u0001\u0000"+
		"\u0000\u0000no\u0001\u0000\u0000\u0000op\u0001\u0000\u0000\u0000pq\u0006"+
		"\u0010\u0000\u0000q\"\u0001\u0000\u0000\u0000\u0006\u0000X^dfn\u0001\u0006"+
		"\u0000\u0000";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}