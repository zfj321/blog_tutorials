lexer grammar SQLLexer;

@header {
    package com.example.parser;
}

// Tokens
SELECT: [Ss][Ee][Ll][Ee][Cc][Tt];
FROM: [Ff][Rr][Oo][Mm];
WHERE: [Ww][Hh][Ee][Rr][Ee];
ORDER: [Oo][Rr][Dd][Ee][Rr];
BY: [Bb][Yy];
DESC: [Dd][Ee][Ss][Cc];
AND: [Aa][Nn][Dd];
OR: [Oo][Rr];
EQUAL: '=';
STAR: '*';
COMMA: ',';
SEMICOLON: ';';
QUOTE: '\'';

// Identifiers and literals
ID: [a-zA-Z_][a-zA-Z_0-9]*;
INT: [0-9]+;
STRING: QUOTE (~[\r\n'] | '\'\'' )* QUOTE;

// Whitespace and comments
WS: [ \t\r\n]+ -> skip;